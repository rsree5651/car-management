/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CarDetail {
    private Integer id;
    private Integer manufactureId;
    private String carName;
    private String carType;
    private String carColour;

    public CarDetail(Integer id, Integer manufactureId, String carName, String carType, String carColour) {
        this.id = id;
        this.manufactureId = manufactureId;
        this.carName = carName;
        this.carType = carType;
        this.carColour = carColour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getManufactureId() {
        return manufactureId;
    }

    public void setManufactureId(Integer manufactureId) {
        this.manufactureId = manufactureId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCarColour() {
        return carColour;
    }

    public void setCarColour(String carColour) {
        this.carColour = carColour;
    }
    private static final Logger LOG = Logger.getLogger(CarDetail.class.getName());

    public static Logger getLOG() {
        return LOG ;
    }  
   

  }
