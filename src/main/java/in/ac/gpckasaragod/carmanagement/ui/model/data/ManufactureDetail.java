/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ManufactureDetail {
    private Integer id;
    private String name;

    public ManufactureDetail(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private static final Logger LOG = Logger.getLogger(ManufactureDetail.class.getName());

    public static Logger getLOG() {
        return LOG;
    }
    
}
