/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.service.impl;

import in.ac.gpckasaragod.carmanagement.service.ManufactureService;
import in.ac.gpckasaragod.carmanagement.ui.ManufactureDetailsForm;
import in.ac.gpckasaragod.carmanagement.ui.model.data.ManufactureDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ManufactureServiceImpl extends ConnectionServiceImpl implements ManufactureService {

    @Override
    public String saveManufactureDetails(String name) {
        try {

            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO MANUFACTURE_DETAILS (NAME) VALUES "
                    + "('" + name + "')";
            System.err.print("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "saved failed";
            } else {
                return "saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManufactureServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "saved failed";
    }

    @Override
    public ManufactureDetail readManufactureDetails(Integer id) {
        ManufactureDetail manufactureDetail = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM  MANUFACTURE_DETAILS WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                manufactureDetail = new ManufactureDetail(id, name);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ManufactureServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return manufactureDetail;
    }

    @Override
    public List<ManufactureDetail> getAllManufactureDetails() {
        List<ManufactureDetail> manufactureDetails = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM MANUFACTURE_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String name = resultSet.getString("NAME");
                Integer id = resultSet.getInt("ID");
                ManufactureDetail manufacturedetail = new ManufactureDetail(id, name);
                manufactureDetails.add(manufacturedetail);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ManufactureServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return manufactureDetails;
    }

    @Override
    public String deleteManufactureDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM MANUFACTURE_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Delete successfully";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return "Delete failed";
        }
    }

    @Override
    public String updateManufactureDetail(ManufactureDetail manufacturedetails) {
        // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE MANUFACTURE_DETAILS SET NAME ='" + manufacturedetails.getName() + "'WHERE ID=" + manufacturedetails.getId();
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Update successfully";
            }
        } catch (SQLException ex) {
            return "Update failed";
        }
    }

}
