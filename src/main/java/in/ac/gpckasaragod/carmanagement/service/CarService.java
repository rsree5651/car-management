/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.service;

import in.ac.gpckasaragod.carmanagement.ui.model.data.CarDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface CarService {
   public String saveCarDetails(Integer id,Integer manufactureid,String carName,String CarType,String carColour);
   public CarDetail readCarDetails(Integer id) ;
   public List<CarDetail>getAllCarDetails();
   public String updateCarDetails(CarDetail carDetails);
   public String deleteCarDetails(Integer id);
    
    
}
