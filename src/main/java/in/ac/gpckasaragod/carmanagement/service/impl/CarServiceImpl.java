/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.service.impl;

import in.ac.gpckasaragod.carmanagement.service.CarService;
import in.ac.gpckasaragod.carmanagement.ui.model.data.CarDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CarServiceImpl extends ConnectionServiceImpl implements CarService {
   
    @Override
    public String saveCarDetails(Integer carid,Integer manufactureid,String carname,String cartype,String carcolour){
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query="INSERT INTO CAR_DETAILS (CAR_ID,MANUFACTURE_ID,CAR_NAME,CAR_TYPE,CAR_COLOUR) VALUES"
                    +"('"+carid+"','"+manufactureid+"','"+carname+"','"+cartype+"','"+carcolour+"')";
            System.err.print("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return"saved failed";
            }else{
                return"saved successfully";
                
            }
            } catch (SQLException ex) {
                Logger.getLogger(CarServiceImpl.class.getName()).log(Level.SEVERE, null,ex);
                
            }
              return "save failed";
         }

    @Override
    public CarDetail readCarDetails(Integer id) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
              String query = "SELECT * FROM  CAR_DETAILS WHERE ID=" + id;
              ResultSet resultSet = statement.executeQuery(query);
              while (resultSet.next()) {
                  Integer id = resultSet.get Id("ID");
                  Integer id = resultSet.get Id("MANUFACTURE ID");
                  
                  
              }
            System.err.print("Query:"+query);
        } catch (SQLException ex) {
            Logger.getLogger(CarServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<CarDetail> getAllCarDetails() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updateCarDetails(CarDetail carDetails) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteCarDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
           
      
    
}
