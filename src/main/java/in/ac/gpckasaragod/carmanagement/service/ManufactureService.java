/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.carmanagement.service;


import in.ac.gpckasaragod.carmanagement.ui.model.data.ManufactureDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface ManufactureService {
  public String saveManufactureDetails(String name);
public ManufactureDetail readManufactureDetails(Integer id);
  
public List<ManufactureDetail>getAllManufactureDetails();
public String updateManufactureDetail(ManufactureDetail manufacturedetails);
public String deleteManufactureDetails(Integer id) ; 


  
}
